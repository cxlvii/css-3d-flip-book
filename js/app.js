var diaryApp = angular.module("Diary", ['LocalStorageModule', 'ngAnimate']);


diaryApp.config(function(localStorageServiceProvider){
  localStorageServiceProvider
      .setPrefix('diary')
      .setStorageType('sessionStorage')
      .setNotify(true, true);
});

diaryApp.controller("globalCtrl", function ($scope, $rootScope, localStorageService) {

  // Basic settings
  $rootScope.settings = {
    diary: {
      pagesCount: 12
    },
    page: {
      rowsCount: 24
    }
  };

  // Table row template
  var createEmptyRow = function(){
    return {id: null, date: '', item: '', price: null, count: '', total: ''};
  };

  // If no records found in local storage -> create empty DB
  var createEmptyDB = function(){
    // count how many sheets do we need for 3d view
    var sheetsCount = Math.ceil($rootScope.settings.diary.pagesCount / 2);
    // make an array with empty cells for pages
    var db = { table: Array.apply(null, Array(sheetsCount)).map(function () {}) };
    // counter for rows id
    var idCounter = 0;
    for (var i = 0; i < sheetsCount; i++){
      // create front and back sheet pages
      db.table[i] = [null,null];
      for (var j = 0; j < db.table[i].length; j++) {
        var page = [];
        // fill page with rows
        for (var y = 1; y < $rootScope.settings.page.rowsCount + 1; y++) {
          var row = createEmptyRow();
          row.id = idCounter;
          idCounter++;
          page.push(row);
        }
        db.table[i][j] = page;
      }
    }
    // console.log(db);
    return db;
  };



  $rootScope.localDB = (localStorageService.get('localDB')) ? localStorageService.get('localDB') : createEmptyDB();

  // bind Diary DB to local storage
  localStorageService.bind($scope, 'localDB', $rootScope.localDB);

  // reset local storage func
  $scope.reset = function(){
    localStorageService.clearAll();
    window.location.reload();
  }

  // $scope.sortType = 'date';
  // $scope.sortReverse = true;

  $scope.orderBy = function(_type){
    $scope.sortType = _type;
  };

});




// TweenMax animation triggered on class change
diaryApp.animation('.is-flipped', function(){
  return {
    addClass : function(element, className, done) {
      // RTL animation
      if(element.hasClass('is-odd')){
        TweenMax.to(element,1,{rotationY: -180})
      }else{
        TweenMax.to(element,1,{rotationY: 0})
      }
    },
    removeClass : function(element, className, done) {
      // LTR animation
      var z = parseInt(element[0].style.zIndex),
          overrided = true;

      // set zIndex overkill
      element[0].style.zIndex = 10000;

      // wait for page get in center, then set native z-index
      function onHalfComplete(progress){
        if(Math.round(progress*100) >= 50 && overrided){
          overrided = false;
          element[0].style.zIndex = z;
        }
      }
      if(element.hasClass('is-odd')){
        TweenMax.to(element,1,{rotationY: 0, onUpdate: function(){
          onHalfComplete(this.progress());
        }})
      }else{
        TweenMax.to(element,1,{rotationY: 180, onUpdate: function(){
          onHalfComplete(this.progress());
        }})
      }
    }
  };
});



// Page flip buttons
diaryApp.controller("pageCtrl", function ($scope, $rootScope) {

  var pgCount = Math.ceil($rootScope.settings.diary.pagesCount / 2);
  $scope.stateFlipped = false;
  $scope.zIndex = pgCount - 2 - $scope.$index;


  $scope.flip = function(e, sheet){
    $scope.stateFlipped = !$scope.stateFlipped;
    if($scope.stateFlipped){
      $scope.zIndex = pgCount - 1 + $scope.$index;
    }else{
      $scope.zIndex = pgCount - 2 - $scope.$index;
    }
  };

});


// auto on product update
diaryApp.controller("itemCtrl", function ($scope, $filter, $rootScope) {


  $scope.onRowUpdate = function(cell, page, sheet){
    // console.log(cell,page,sheet);
    var row = $rootScope.localDB.table[sheet][page][cell];

    row.date = (row.date === '') ? new Date() : row.date;
    // row.price = $filter('currency')(row.price);

    if(row.price !== '' && row.count === ''){
      row.count = 1;
    }

    if(row.price !== '' && row.count !== ''){
      row.total = $filter('currency')(row.price * row.count);
    }
  };

  $scope.rowDelete = function(cell, page, sheet){

  };
});


console.log('SRC: https://bitbucket.org/cxlvii/css-3d-flip-book/downloads');













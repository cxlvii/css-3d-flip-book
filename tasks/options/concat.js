module.exports = {
  dist: {
    src: [
      'js/libs/*.js',
      'js/main.js'
    ],
    dest: 'js/main.min.js'
  }
}
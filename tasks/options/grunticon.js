module.exports = {
  myIcons: {
    files: [{
      expand: true,
      cwd: 'i/icons',
      src: ['*.svg', '*.png'],
      dest: "i/icons-fallbacks/"
    }],
    options: {
    }
  }
}
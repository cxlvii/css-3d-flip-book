module.exports = {
  dynamic: {
    files: [{
      expand: true,
      cwd: 'i/',
      src: ['**/*.{png,jpg,gif}'],
      dest: 'i/'
    }]
  }
}
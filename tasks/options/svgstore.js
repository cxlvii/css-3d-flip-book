module.exports = {
  options: {
          prefix: 'icon-',
          cleanup: true,
          svg: {
              viewBox: '0 0 32 32',
              class: 'is-hidden'
          }
      },
      default: {
          files: {
              'i/svg-sprite.svg': ['i/icons/*.svg']
           }
      }
}